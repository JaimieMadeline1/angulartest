import { Component } from "@angular/core";
import {
  SELECTION_TYPE_SELECTED,
  SELECTION_TYPE_AVAILABLE,
  SELECTION_TYPE_ADD
} from "../constants/courseSelectionTypes.js";
import { CheckBox } from "../interfaces/checkbox";
import { YearlyCoursePlanner } from "../interfaces/coursesIntefrace";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  currNumber: number = 14;
  maxNumber: number = 30;
  schoolYear: number = 2017;
  checkBoxes: CheckBox[] = [
    { id: "selector-check-" + 1, label: "All" },
    { id: "selector-check-" + 2, label: "Earned" },
    { id: "selector-check-" + 3, label: "Planned" },
    { id: "selector-check-" + 4, label: "Remaining" }
  ];
  currentYear: number = 8;
  coursePlanner: YearlyCoursePlanner[] = [
    {
      yearNumber: 9,
      creditsPlanned: 1,
      courses: [
        {
          courseSelectionType: SELECTION_TYPE_SELECTED,
          courseTitle: "English",
          courseCode: "ENG1PI",
          courseDescription: "English 9",
          creditValue: 1
        },
        {
          courseSelectionType: SELECTION_TYPE_SELECTED,
          courseTitle: "Functions",
          courseCode: "MAT1U1",
          courseDescription: "Math 9",
          creditValue: 1,
          preReqWarning: true
        },
        {
          courseSelectionType: SELECTION_TYPE_ADD,
          courseTitle: "Science 9",
          creditValue: 1
        },
        {
          courseSelectionType: SELECTION_TYPE_ADD,
          courseTitle: "Geography",
          creditValue: 1
        },
        {
          courseSelectionType: SELECTION_TYPE_ADD,
          courseTitle: "French",
          creditValue: 1
        },
        {
          courseSelectionType: SELECTION_TYPE_ADD,
          courseTitle: "Elective",
          customText: "16 credits remaining",
          creditValue: 1
        }
      ]
    },
    {
      yearNumber: 10,
      creditsPlanned: 0,
      courses: []
    },
    {
      yearNumber: 11,
      creditsPlanned: 0,
      courses: []
    },
    {
      yearNumber: 12,
      creditsPlanned: 0,
      courses: []
    }
  ];
}
