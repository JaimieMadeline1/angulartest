import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CourseSelectorMenuComponent } from './course-selector-menu/course-selector-menu.component';

@NgModule({
  declarations: [
    AppComponent,
    CourseSelectorMenuComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
