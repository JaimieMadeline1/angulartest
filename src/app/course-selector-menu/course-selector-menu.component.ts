import { Component, OnInit, Input } from "@angular/core";
import {
  SELECTION_TYPE_SELECTED,
  SELECTION_TYPE_AVAILABLE,
  SELECTION_TYPE_ADD
} from "../../constants/courseSelectionTypes.js";
import { YearlyCoursePlanner, Course } from "../../interfaces/coursesIntefrace.js";

@Component({
  selector: "app-course-selector-menu",
  templateUrl: "./course-selector-menu.component.html",
  styleUrls: ["./course-selector-menu.component.scss"]
})
export class CourseSelectorMenuComponent implements OnInit {
  @Input() courseMenuDetails: YearlyCoursePlanner;
  @Input() currentYear: number;
  maxCourses: number = 8;
  hovering: String = "";

  constructor() {}

  ngOnInit() {
    if (this.courseMenuDetails.courses.length < this.maxCourses) {
      let emptySlots: number =
        this.maxCourses - this.courseMenuDetails.courses.length;
      for (let i = 0; i < emptySlots; i++) {
        const newCourse: Course = {
          courseSelectionType: SELECTION_TYPE_AVAILABLE
        };

        this.courseMenuDetails.courses.push(newCourse);
      }
    }
  }
  mouseEnter(course) {
    if (course && course.courseCode && course.preReqWarning == true) {
      this.hovering = course.courseCode;
    }
  }
  mouseLeave(course) {
    this.hovering = "";
  }
}
