import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseSelectorMenuComponent } from './course-selector-menu.component';

describe('CourseSelectorMenuComponent', () => {
  let component: CourseSelectorMenuComponent;
  let fixture: ComponentFixture<CourseSelectorMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseSelectorMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseSelectorMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
