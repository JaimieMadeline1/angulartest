export interface CheckBox {
  id: String;
  label: String;
}