export interface YearlyCoursePlanner {
  yearNumber?: number;
  creditsPlanned?: number;
  courses: Course[];
}

export interface Course {
  courseSelectionType: String;
  courseTitle?: String;
  courseCode?: String;
  courseDescription?: String;
  creditValue?: number;
  preReqWarning?: Boolean;
  customText?: String;
}
